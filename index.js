'use strict';

/**
 * Store for logs
 * @typedef {Object} Store
 * @property {string} name - name of npm-module
 * @property {Object} config
 */

const os = require('os');


/**
 * Metrics module
 * @class
 */
class Metrics {

  /**
   *
   * @param {Object} config
   * @param {string} [config.prefix]
   * @param {Store[]} [config.metricTargets] - stores options
   * @param {string[]} [config.enabledMetrics] - enabled stores names
   */
  constructor(config) {
    const enabledMetrics = (Array.isArray(config.enabledMetrics)) ? config.enabledMetrics : [];

    this.isEnabledConsole = (enabledMetrics.indexOf('console') !== -1);
    this.stores = [];

    if (config.prefix && typeof config.prefix === 'string') {
      this.prefix = config.prefix.replace(/%hostname%/, os.hostname()) + '.';
    } else {
      this.prefix = '';
    }

    if (Array.isArray(config.metricTargets)) {
      for (let store of config.metricTargets) {
        if (enabledMetrics.indexOf(store.name) !== -1) {
          const storeClass = require(store.name);
          this.stores.push(new storeClass(store.config));
        }
      }
    }

  }

  /**
   * log time of action
   * @param {string} metric
   * @param {number} time
   * @param {*} [data] - additional data
   */
  logTime(metric, time, data) {
    const metricName = `${this.prefix}${metric}`;

    if (this.isEnabledConsole) {
      console.log(new Date().toJSON(), metricName, time, JSON.stringify(data));
    }

    for (let store of this.stores) {
      store.logTime(metricName, time, data);
    }

  }

  /**
   * log count of action
   * @param {string} metric
   * @param {number} count
   * @param {*} [data] - additional data
   */
  logCount(metric, count, data) {
    const metricName = `${this.prefix}${metric}`;

    if (this.isEnabledConsole) {
      console.log(new Date().toJSON(), metricName, count, JSON.stringify(data));
    }

    for (let store of this.stores) {
      store.logCount(metricName, count, data);
    }

  }

}

module.exports = Metrics;