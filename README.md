# Metrics module
The Metrics module for logging (node.js > 4.*)

#### For example log response time from Google with StatsD 
#### include 'console' to config.enabledMetrics for pm2 logs
``` js
const Metrics = require('xpm-metrics');
const metrics = new Metrics({
  metricTargets: [
    {
      name: 'xpm-metrics-statsd',
      config: {
        host: 'example.host',
        port: 8124
      }
    }
  ],
  enabledMetrics: ['console', 'xpm-metrics-statsd'],
  prefix: 'test_prefix.%hostname%'
});
const request = require('request-promise');
const params = {
    uri: 'https://www.google.com',
    resolveWithFullResponse: true,
    time: true
};

request(params)
    .then((res) => metrics.logTime('google_response', res.elapsedTime, 'test'));
```
#### find obtained logs with grep
```
grep -h -r 'google_response' ~/.pm2/logs/
```
result:
```
2017-02-03T09:25:01.329Z google_response 561 "test"
2017-02-03T09:34:32.153Z google_response 858 "test"
2017-02-03T09:36:38.098Z google_response 357 "test"
```
#### print in CSV-format
```
grep -h -r 'google_response' ~/.pm2/logs/ | awk '{print $1 ";" $2 ";" $3 ";" $4}'
```
result:
```
2017-02-03T09:25:01.329Z;google_response;561;"test"
2017-02-03T09:34:32.153Z;google_response;858;"test"
2017-02-03T09:36:38.098Z;google_response;357;"test"
```

For metricTargets can be used:
- [xpm-metrics-graylog](https://www.npmjs.com/package/xpm-metrics-graylog)
- [xpm-metrics-statsd](https://www.npmjs.com/package/xpm-metrics-statsd)
